/* Copyright Steve Rabin, 2013. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2013"
 */

#include <Stdafx.h>
#include "BehaviorTrees/BlackBoards/RatBlackBoard.h"

//#define UNIT_TESTING

World::World(void)
    : m_initialized(false)
{
}

World::~World(void)
{
}

void World::InitializeSingletons(void)
{
    //Create Singletons
    m_clock = &g_clock;
    m_database = &g_database;
    m_msgroute = &g_msgroute;
    m_debuglog = &g_debuglog;
    m_random = &g_random;
    m_gv_database = &g_global_variable;
}

void World::Initialize(CMultiAnim* pMA, std::vector<CTiny*>* pv_pChars, CSoundManager* pSM, double dTimeCurrent)
{
    if (!m_initialized)
    {
        m_initialized = true;


        #ifdef UNIT_TESTING

		//Create unit test game objects
		GameObject* unittest1 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest1" );
		GameObject* unittest2 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest2" );
		GameObject* unittest3a = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3a" );
		GameObject* unittest3b = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3b" );
		GameObject* unittest4 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest4" );
		GameObject* unittest5 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest5" );
		GameObject* unittest6 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest6" );
		
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		unittest1->CreateBody( 100, pos );
		unittest2->CreateBody( 100, pos );
		unittest3a->CreateBody( 100, pos );
		unittest3b->CreateBody( 100, pos );
		unittest4->CreateBody( 100, pos );
		unittest5->CreateBody( 100, pos );
		unittest6->CreateBody( 100, pos );

		unittest1->CreateStateMachineManager();
		unittest2->CreateStateMachineManager();
		unittest3a->CreateStateMachineManager();
		unittest3b->CreateStateMachineManager();
		unittest4->CreateStateMachineManager();
		unittest5->CreateStateMachineManager();
		unittest6->CreateStateMachineManager();
		
		g_database.Store( *unittest1 );
		g_database.Store( *unittest2 );
		g_database.Store( *unittest3a );
		g_database.Store( *unittest3b );
		g_database.Store( *unittest4 );
		g_database.Store( *unittest5 );
		g_database.Store( *unittest6 );

		//Give the unit test game objects a state machine
		unittest1->GetStateMachineManager()->PushStateMachine( *new UnitTest1( *unittest1 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest2->GetStateMachineManager()->PushStateMachine( *new UnitTest2a( *unittest2 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3a->GetStateMachineManager()->PushStateMachine( *new UnitTest3a( *unittest3a ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3b->GetStateMachineManager()->PushStateMachine( *new UnitTest3b( *unittest3b ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest4->GetStateMachineManager()->PushStateMachine( *new UnitTest4( *unittest4 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest5->GetStateMachineManager()->PushStateMachine( *new UnitTest5( *unittest5 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest6->GetStateMachineManager()->PushStateMachine( *new UnitTest6( *unittest6 ), STATE_MACHINE_QUEUE_0, TRUE );

        #else
        g_trees.Initialize(); // initialize behavior tree

        std::weak_ptr<BaseGlobalVar> global_cat
                = g_global_variable.RegisterGlobalVar<GameObjectGVar>("Cat");
        {
            //Create game objects
            char name[10] = "My Cat";
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = g_random.RangeFloat();
            pos.z = g_random.RangeFloat();
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(1, 1, 1);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 1.0f, 1.0f, 1.0f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "Example"); // register agent to behavior tree
            g_trees.GetAgentData(name).InitCatBlackBoard(npc); // initialize local blackboard for each tiny

            if (const auto& cat_ptr = global_cat.lock())
            {
                cat_ptr->Cast<GameObjectGVar>()->m_GameObject = npc;
            }
        }
        {
            //Create game objects
            char name[10] = "Cat Food";
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = 0.5f;
            pos.z = 0.5f;
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(5, 0.5f, 3);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 20.0f, 20.0f, 0.05f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "CatFood"); // register agent to behavior tree
            g_trees.GetAgentData(name).InitCatBlackBoard(npc); // initialize local blackboard for each tiny

            std::weak_ptr<BaseGlobalVar> global_cheese
                = g_global_variable.RegisterGlobalVar<GameObjectGVar>("Cheese");
            if (auto cheese_ptr = global_cheese.lock())
            {
                cheese_ptr->Cast<GameObjectGVar>()->m_GameObject = npc;
            }
        }

        for (int i = 0; i < 20; ++i)
        {
            //Create game objects
            char name[10] = "Rat";
            sprintf(name, "%s%d", name, i);
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            npc->m_FedColors = true;
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = g_random.RangeFloat(0.2f, 1);
            pos.z = g_random.RangeFloat(0.2f, 1);
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(0.4f, 0.4f, 0.4f);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 0.0f, 0.0f, 0.0f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "Rat"); // register agent to behavior tree
            BT::AgentBTData& agent_bt_data = g_trees.GetAgentData(name);
            agent_bt_data.InitRatBlackBoard(npc); // initialize local blackboard for each tiny
            agent_bt_data.GetLocalBlackBoard<BT::RatBlackBoard>()->m_Cat = global_cat;
        }

        {
            //Create game objects
            char name[20] = "Rat Pure Random";
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            npc->m_FedColors = true;
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = g_random.RangeFloat(0.2f, 1);
            pos.z = g_random.RangeFloat(0.2f, 1);
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(0.4f, 0.4f, 0.4f);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 0.0f, 10.0f, 0.0f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "Pure Random Rat"); // register agent to behavior tree
            BT::AgentBTData& agent_bt_data = g_trees.GetAgentData(name);
            agent_bt_data.InitRatBlackBoard(npc); // initialize local blackboard for each tiny
            agent_bt_data.GetLocalBlackBoard<BT::RatBlackBoard>()->m_Cat = global_cat;
        }

        {
            //Create game objects
            char name[20] = "Random Rat";
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = g_random.RangeFloat(0.2f, 1);
            pos.z = g_random.RangeFloat(0.2f, 1);
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(0.4f, 0.4f, 0.4f);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 0.0f, 0.0f, 10.0f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "Random Rat"); // register agent to behavior tree
            BT::AgentBTData& agent_bt_data = g_trees.GetAgentData(name);
            agent_bt_data.InitRatBlackBoard(npc); // initialize local blackboard for each tiny
            agent_bt_data.GetLocalBlackBoard<BT::RatBlackBoard>()->m_Cat = global_cat;
        }
        {
            //Create game objects
            char name[20] = "Dumb Random";
            GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, name);
            D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
            pos.x = g_random.RangeFloat(0.2f, 1);
            pos.z = g_random.RangeFloat(0.2f, 1);
            npc->CreateBody(100, pos);
            npc->GetBody().SetScale(0.4f, 0.4f, 0.4f);
            npc->CreateMovement();
            npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 10.0f, 0.0f, 0.0f); //Color if needed
            g_database.Store(*npc);

            g_trees.Register(name, "Dumb Rat"); // register agent to behavior tree
            BT::AgentBTData& agent_bt_data = g_trees.GetAgentData(name);
            agent_bt_data.InitRatBlackBoard(npc); // initialize local blackboard for each tiny
            agent_bt_data.GetLocalBlackBoard<BT::RatBlackBoard>()->m_Cat = global_cat;
        }
        #endif
    }
}


void World::PostInitialize()
{
    g_database.Initialize();
}


void World::Update(float dt)
{
    g_clock.MarkTimeThisTick();
    g_database.Update();
    g_trees.Update(dt);
}

void World::Animate(double dTimeDelta)
{
    g_database.Animate(dTimeDelta);
}

void World::AdvanceTimeAndDraw(IDirect3DDevice9* pd3dDevice, D3DXMATRIX* pViewProj, double dTimeDelta,
                               D3DXVECTOR3* pvEye)
{
    g_database.AdvanceTimeAndDraw(pd3dDevice, pViewProj, dTimeDelta, pvEye);
}

void World::RestoreDeviceObjects(LPDIRECT3DDEVICE9 pd3dDevice)
{
    return (g_database.RestoreDeviceObjects(pd3dDevice));
}

void World::InvalidateDeviceObjects(void)
{
    g_database.InvalidateDeviceObjects();
}
