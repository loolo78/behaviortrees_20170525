﻿#pragma once


struct BaseGlobalVar
{
    explicit BaseGlobalVar(const std::string& name);
    virtual ~BaseGlobalVar();

    template <class T>
    T* Cast()
    {
        return dynamic_cast<T*>(this);
    }

    std::string m_Name;
};

struct VectorGVar : BaseGlobalVar
{
    explicit VectorGVar(const std::string& name);
    D3DXVECTOR3 m_Vector;
};

struct FloatGVar : BaseGlobalVar
{
    explicit FloatGVar(const std::string& name);
    float m_Float;
};

struct GameObjectGVar : BaseGlobalVar
{
    explicit GameObjectGVar(const std::string& name);
    GameObject* m_GameObject = nullptr;
};


class GlobalVariableDatabase
{
public:
    void DeleteGlobalVariable(const std::string& name);
    std::weak_ptr<BaseGlobalVar> GetGlobalVariable(const std::string& name);

    /**
     * @brief 
     * @tparam T GlobalVar struct
     * @return 
     */
    template <class T>
    std::weak_ptr<BaseGlobalVar> RegisterGlobalVar(const std::string& name);
    std::weak_ptr<BaseGlobalVar> RegisterGlobalVar(const std::string& name,
                                                   const std::shared_ptr<BaseGlobalVar>& instance);

private:
    using GVDatabase = std::unordered_map<std::string, std::shared_ptr<BaseGlobalVar>>;
    GVDatabase m_gvDatabase;
};


template <class T>
std::weak_ptr<BaseGlobalVar> GlobalVariableDatabase::RegisterGlobalVar(const std::string& name)
{
    return static_cast<std::weak_ptr<BaseGlobalVar>>(
        m_gvDatabase[name] = std::make_shared<T>(name)
    );
}

