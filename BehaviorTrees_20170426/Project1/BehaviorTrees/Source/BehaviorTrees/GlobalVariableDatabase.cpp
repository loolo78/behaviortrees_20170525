﻿#include "Stdafx.h"
#include "GlobalVariableDatabase.h"

BaseGlobalVar::BaseGlobalVar(const std::string& name)
    : m_Name(name)
{
}

BaseGlobalVar::~BaseGlobalVar()
{
}

VectorGVar::VectorGVar(const std::string& name)
    : BaseGlobalVar(name)
{
}

GameObjectGVar::GameObjectGVar(const std::string& name)
    : BaseGlobalVar(name)
{
}

void GlobalVariableDatabase::DeleteGlobalVariable(const std::string& name)
{
    m_gvDatabase.erase(m_gvDatabase.find(name));
}

std::weak_ptr<BaseGlobalVar> GlobalVariableDatabase::GetGlobalVariable(const std::string& name)
{
    const auto var = m_gvDatabase.find(name);
    if (var == m_gvDatabase.end())
    {
        return std::weak_ptr<BaseGlobalVar>(); // = NULLPTR
    }
    else
    {
        return var->second;
    }
}

std::weak_ptr<BaseGlobalVar> GlobalVariableDatabase::RegisterGlobalVar(
    const std::string& name,
    const std::shared_ptr<BaseGlobalVar>& instance)
{
    return m_gvDatabase[name] = instance;
}
