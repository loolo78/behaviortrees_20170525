﻿#pragma once

namespace BT
{
    class L_IsCloseToCat : public LeafNode
    {
    protected:
        Status OnEnter(NodeData* nodedata_ptr) override;
    public:
        ~L_IsCloseToCat() override;
    };
    
}
