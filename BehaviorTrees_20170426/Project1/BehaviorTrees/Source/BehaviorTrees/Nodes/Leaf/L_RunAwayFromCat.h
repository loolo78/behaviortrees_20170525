﻿#pragma once

#include <BehaviorTrees/BehaviorTreesShared.h>

namespace BT
{
    class L_RunAwayFromCat : public LeafNode
    {
    protected:
        Status OnEnter(NodeData* nodedata_ptr) override;
        Status OnUpdate(float dt, NodeData* nodedata_ptr) override;
    public:
    };
}
