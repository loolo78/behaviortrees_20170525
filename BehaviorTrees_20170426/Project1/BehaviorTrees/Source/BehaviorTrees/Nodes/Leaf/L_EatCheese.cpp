﻿#include "Stdafx.h"
#include "L_EatCheese.h"

namespace BT
{
    Status L_EatCheese::OnEnter(NodeData* nodedata_ptr)
    {
        if (auto cheese_gr = g_global_variable.GetGlobalVariable("Cheese").lock())
        {
            if (auto cheese = cheese_gr->Cast<GameObjectGVar>()->m_GameObject)
            {
                D3DXVECTOR3 cheese_pos = cheese->GetBody().GetPos();
                D3DXVECTOR3 my_pos = nodedata_ptr->GetAgentData().GetGameObject()->GetBody().GetPos();
                D3DXVECTOR3 dist_vec = cheese_pos - my_pos;
                const float dist = D3DXVec3Length(&dist_vec);
                if (dist > 1.1f)  // too far
                {
                    return BT_FAILURE;
                }
            }
        }
        nodedata_ptr->GetAgentData().GetGameObject()->SetFed();
        return BT_SUCCESS;
    }
}
