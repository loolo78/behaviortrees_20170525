﻿#pragma once

namespace BT
{
    class L_RunAwayFromCheese : public LeafNode
    {
    protected:
        Status OnEnter(NodeData* nodedata_ptr) override;
        Status OnUpdate(float dt, NodeData* nodedata_ptr) override;
    public:
    
    };
    
}
