﻿#include "Stdafx.h"
#include "L_GoToCheese.h"

using namespace BT;

Status L_GoToCheese::OnEnter(NodeData* nodedata_ptr)
{

    bool cheese_exists = false;
    D3DXVECTOR3 cheese_pos;
    if (auto cheese_gr = g_global_variable.GetGlobalVariable("Cheese").lock())
    {
        if (auto cheese = cheese_gr->Cast<GameObjectGVar>()->m_GameObject)
        {
            cheese_pos = cheese->GetBody().GetPos();
            cheese_exists = true;
        }
    }

    LeafNode::OnEnter(nodedata_ptr);

	AgentBTData &agentdata = nodedata_ptr->GetAgentData();
	GameObject *self = agentdata.GetGameObject();

	if (cheese_exists)
	{
		self->SetTargetPOS(cheese_pos);
		self->SetSpeedStatus(TS_JOG);
		SetTinySpeed(self);
		return Status::BT_RUNNING;
	}

    return Status::BT_FAILURE;
}

Status L_GoToCheese::OnUpdate(float dt, NodeData* nodedata_ptr)
{
	LeafNode::OnUpdate(dt, nodedata_ptr);

	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();

	if (IsNear(self->GetBody().GetPos(), self->GetTargetPOS()))
		return Status::BT_SUCCESS;

    return Status::BT_RUNNING;
}
