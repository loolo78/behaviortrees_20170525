﻿#include "Stdafx.h"
#include "L_IsHungry.h"

using namespace BT;
Status L_IsHungry::OnEnter(NodeData* nodedata_ptr)
{
    if (nodedata_ptr->GetAgentData().GetGameObject()->IsFed())
    {
        return BT_FAILURE;
    }
    else
    {
        return BT_SUCCESS;
    }
}
