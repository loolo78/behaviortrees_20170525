﻿#pragma once

namespace BT
{
    class L_GoToCheese : public LeafNode
    {
    protected:
        Status OnEnter(NodeData* nodedata_ptr) override;
        Status OnUpdate(float dt, NodeData* nodedata_ptr) override;
    };
    
}
