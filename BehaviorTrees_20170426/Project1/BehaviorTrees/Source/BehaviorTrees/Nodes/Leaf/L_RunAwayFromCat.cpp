﻿#include "Stdafx.h"
#include "L_RunAwayFromCat.h"
#include "BehaviorTrees/BlackBoards/RatBlackBoard.h"

namespace BT
{

    Status L_RunAwayFromCat::OnEnter(NodeData* nodedata_ptr)
    {
        LeafNode::OnEnter(nodedata_ptr);

        GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();

        if (auto cat = nodedata_ptr->GetAgentData().GetLocalBlackBoard<RatBlackBoard>()->m_Cat.lock())
        {
            GameObject* cat_gameobject = cat->Cast<GameObjectGVar>()->m_GameObject;
            if (cat_gameobject)
            {
                const D3DXVECTOR3 cat_pos = cat_gameobject->GetBody().GetPos();
                const D3DXVECTOR3 my_pos = self->GetBody().GetPos();

                D3DXVECTOR3 dir = my_pos - cat_pos;

                if (IsNear(cat_pos, my_pos))
                {
                    dir = D3DXVECTOR3{ g_random.RangeFloat(-0.5f, 0.5f), 0, g_random.RangeFloat(-0.5f, 0.5f) };
                }

                float distance = D3DXVec3Length(&dir);
                dir /= distance;
                dir *= 0.5f;

                self->SetTargetPOS(my_pos + dir);
                self->SetSpeedStatus(TS_JOG);
                SetTinySpeed(self);

                return BT_RUNNING;
            }
        }
        else // no cat, yey
        {
            return BT_SUCCESS;
        }

        return BT_SUCCESS;

    }

    Status L_RunAwayFromCat::OnUpdate(float dt, NodeData* nodedata_ptr)
    {
        LeafNode::OnUpdate(dt, nodedata_ptr);


        GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();

        if (IsNear(self->GetBody().GetPos(), self->GetTargetPOS()))
            return BT_SUCCESS;

        if (self->GetBody().GetPos().x < 0 || self->GetBody().GetPos().x > 1)
            return BT_FAILURE;
        if (self->GetBody().GetPos().z < 0 || self->GetBody().GetPos().z > 1)
            return BT_FAILURE;

        return BT_RUNNING;
    }

}
