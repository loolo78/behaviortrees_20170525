﻿#include "Stdafx.h"
#include "L_IsCloseToCat.h"

namespace BT
{

    Status L_IsCloseToCat::OnEnter(NodeData* nodedata_ptr)
    {
        if (auto cat_gv = g_global_variable.GetGlobalVariable("Cat").lock())
        {
            auto cat = cat_gv->Cast<GameObjectGVar>();
            if (cat && cat->m_GameObject)
            {
                D3DXVECTOR3 cat_pos = cat->m_GameObject->GetBody().GetPos();
                D3DXVECTOR3 my_pos = nodedata_ptr->GetAgentData().GetGameObject()->GetBody().GetPos();
                D3DXVECTOR3 dist_vec = cat_pos - my_pos;
                float dist = D3DXVec3Length(&dist_vec);

                float run_dist = 0.25f;
                if (auto run_dist_gv = g_global_variable.GetGlobalVariable("Rat Run Dist").lock())
                {
                    if (const auto run_dist_gv2 = run_dist_gv->Cast<FloatGVar>())
                    {
                        run_dist = run_dist_gv2->m_Float;
                    }
                }
                if (dist <= run_dist)
                {
                    return BT_SUCCESS;
                }
            }
        }
        return BT_FAILURE;
    }


    L_IsCloseToCat::~L_IsCloseToCat()
    {
    }
}
