﻿#include "Stdafx.h"
#include "L_GoHungry.h"

using namespace BT;

Status L_GoHungry::OnEnter(NodeData* nodedata_ptr)
{
    nodedata_ptr->GetAgentData().GetGameObject()->SetFed(false);
    return BT_SUCCESS;
}
