﻿#pragma once

namespace BT
{
    // selector node
    class L_MoveToClosestMouse: public LeafNode
    {
    protected:
        // Only run when initializing the node
        void OnInitial(NodeData *nodedata_ptr) override;
        // Only run when entering the node
        Status OnEnter(NodeData *nodedata_ptr) override;
        // Only run when exiting the node
        void OnExit(NodeData *nodedata_ptr) override;
        // Run every frame
        Status OnUpdate(float dt, NodeData *nodedata_ptr) override;
        // Only run when node is in suspended
        Status OnSuspend(NodeData *nodedata_ptr) override;
    };
    
}
