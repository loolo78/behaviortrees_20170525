/******************************************************************************/
/*!
\file		LeafNode.h
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Leaf node logic base class.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#pragma once

#include <BehaviorTrees/BehaviorTreesShared.h>

namespace BT
{
	// leaf node
	class LeafNode : public BehaviorNode
	{
	public:
	protected:
		// Only run when initializing the node
	    void OnInitial(NodeData *nodedata_ptr) override;
		// Only run when entering the node
	    Status OnEnter(NodeData *nodedata_ptr) override;
		// Only run when exiting the node
	    void OnExit(NodeData *nodedata_ptr) override;
		// Run every frame
	    Status OnUpdate(float dt, NodeData *nodedata_ptr) override;
		// Only run when node is in suspended
	    Status OnSuspend(NodeData *nodedata_ptr) override;

		// Verify the node
	    bool Verify(void) override;
	};
}