﻿#include "Stdafx.h"
#include "D_RepeatTwice.h"


namespace BT
{

    Status D_RepeatTwice::OnUpdate(float dt, NodeData* nodedata_ptr)
    {
        const Status child_status = nodedata_ptr->GetChildStatus(0);
        RepeatTwiceData* data = nodedata_ptr->GetLocalBlackBoard<RepeatTwiceData>();

        switch (child_status)
        {
        case BT_SUCCESS: // if child success two times then stop, else reset child and repeat
            if (data->count == 2)
            {
                return BT_SUCCESS;
            }
            else
            {
                ++data->count;
                ResetChildReturnStatus(nodedata_ptr);
                RunChild(true, false, nodedata_ptr);
            }
            break;
        default:;
        }

        return child_status; // suspent, fail and running is handled by child
    }

    void D_RepeatTwice::OnInitial(NodeData* nodedata_ptr)
    {
        nodedata_ptr->InitialLocalBlackBoard<RepeatTwiceData>();
    }

}
