﻿#pragma once

namespace BT
{
    struct RepeatTwiceData : NodeAbstractData
    {
        int count = 0;
    };
    class D_RepeatTwice : public RepeaterNode
    {
    protected:
        Status OnUpdate(float dt, NodeData* nodedata_ptr) override;
        void OnInitial(NodeData* nodedata_ptr) override;
    public:
    
    };
    
}
