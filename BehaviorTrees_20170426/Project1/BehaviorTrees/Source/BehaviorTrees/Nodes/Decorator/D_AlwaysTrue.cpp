﻿#include "Stdafx.h"
#include "D_AlwaysTrue.h"

namespace BT
{

    void D_AlwaysTrue::OnInitial(NodeData* nodedata_ptr)
    {
        RepeaterNode::OnInitial(nodedata_ptr);
    }

    Status D_AlwaysTrue::OnEnter(NodeData* nodedata_ptr)
    {
        return RepeaterNode::OnEnter(nodedata_ptr);
    }

    void D_AlwaysTrue::OnExit(NodeData* nodedata_ptr)
    {
        RepeaterNode::OnExit(nodedata_ptr);
    }

    Status D_AlwaysTrue::OnUpdate(float dt, NodeData* nodedata_ptr)
    {
        Status child_status = nodedata_ptr->GetChildStatus(0);
        if (child_status == BT_RUNNING)
        {
            return BT_RUNNING;
        }
        return BT_SUCCESS;
    }

    Status D_AlwaysTrue::OnSuspend(NodeData* nodedata_ptr)
    {
        return RepeaterNode::OnSuspend(nodedata_ptr);
    }
}
