﻿#include "Stdafx.h"
#include "D_RandomTrueFalse.h"

namespace BT
{
    void D_RandomTrueFalse::OnInitial(NodeData* nodedata_ptr)
    {
        RepeaterNode::OnInitial(nodedata_ptr);
    }

    Status D_RandomTrueFalse::OnEnter(NodeData* nodedata_ptr)
    {
        return RepeaterNode::OnEnter(nodedata_ptr);
    }

    void D_RandomTrueFalse::OnExit(NodeData* nodedata_ptr)
    {
        RepeaterNode::OnExit(nodedata_ptr);
    }

    Status D_RandomTrueFalse::OnUpdate(float dt, NodeData* nodedata_ptr)
    {
        const Status child_status = nodedata_ptr->GetChildStatus(0);
        if (child_status != BT_RUNNING)
        {
            const int rand = g_random.RangeInt(0, 1);
            if (rand)
            {
                return BT_SUCCESS;
            }
            else
            {
                return BT_FAILURE;
            }
        }
        return BT_RUNNING;
    }

    Status D_RandomTrueFalse::OnSuspend(NodeData* nodedata_ptr)
    {
        return RepeaterNode::OnSuspend(nodedata_ptr);
    }
}
