﻿#pragma once


namespace BT
{
    class D_AlwaysTrue : public RepeaterNode
    {
    protected:
        void OnInitial(NodeData* nodedata_ptr) override;
        Status OnEnter(NodeData* nodedata_ptr) override;
        void OnExit(NodeData* nodedata_ptr) override;
        Status OnUpdate(float dt, NodeData* nodedata_ptr) override;
        Status OnSuspend(NodeData* nodedata_ptr) override;
    public:
    
    };
    
}
