﻿#include "Stdafx.h"
#include "RatBlackBoard.h"

using namespace BT;

/*--------------------------------------------------------------------------*
Name:           TinyBlackBoard

Description:    Default constructor.

Arguments:      None.

Returns:        None.
*---------------------------------------------------------------------------*/
RatBlackBoard::RatBlackBoard()
{
    AgentAbstractData();
	m_mouseClick = false;
}

/*--------------------------------------------------------------------------*
Name:           OnMessage

Description:    Handle messages.

Arguments:      None.

Returns:        None.
*---------------------------------------------------------------------------*/
void RatBlackBoard::OnMessage(void)
{
	// default behavior is to drop all messages

	if (m_msgqueue.size())
	{
		MSG_Object &msg = m_msgqueue.front();

		switch (msg.GetName())
		{
		case MSG_Reset:
		{
			m_isreset = true;
		}
			break;

		case MSG_MouseClick:
		{
			m_mouseClick = true;
			m_mousePos = msg.GetVector3Data();
		}
			break;

		default:
			break;
		}

		m_msgqueue.pop();
	}
}