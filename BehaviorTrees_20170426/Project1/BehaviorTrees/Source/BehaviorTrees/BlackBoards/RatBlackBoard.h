﻿#pragma once

namespace BT
{
    // store tiny info
    struct RatBlackBoard : AgentAbstractData
    {
        /* variable */

        // for mouse click only, change later
        bool m_mouseClick;
        D3DXVECTOR3 m_mousePos;
        std::weak_ptr<BaseGlobalVar> m_Cat;

        /* constructors/destructor */

        // Default constructor
        RatBlackBoard();

        /* methods */

        // Handle messages
        void OnMessage(void) override;
    };
}