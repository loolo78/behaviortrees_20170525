/* Copyright Steve Rabin, 2013. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2013"
 */

#pragma once

class GameObject;


class Body
{
public:
    Body(int health, D3DXVECTOR3& pos, GameObject& owner);
    ~Body(void);

    int GetHealth(void) const { return (m_health); }

    void SetHealth(const int health)
    {
        if (health > 0) { m_health = health; }
        else { m_health = 0; }
    }

    bool IsAlive(void) const { return (m_health > 0); }

    void SetSpeed(const float speed) { m_speed = speed; }
    float GetSpeed(void) const { return (m_speed); }

    void SetPos(D3DXVECTOR3& pos) { m_pos = pos; }
    D3DXVECTOR3& GetPos(void) { return (m_pos); }

    void SetDir(D3DXVECTOR3& dir) { m_dir = dir; }
    D3DXVECTOR3& GetDir(void) { return (m_dir); }

    void SetRadius(const float radius) { m_radius = radius; }
    float GetRadius(void) const { return (m_radius); }

    void SetScale(const D3DXVECTOR3 scale) { m_scale = scale; }
    void SetScale(const float x, const float y, const float z) { m_scale = D3DXVECTOR3(x, y, z); }
    D3DXVECTOR3 GetScale(void) const { return m_scale; }

protected:

    GameObject* m_owner;

    int m_health;

    D3DXVECTOR3 m_pos; //Current position
    D3DXVECTOR3 m_dir; //Current facing direction
    float m_speed; //Current movement speed

    float m_radius; //Personal bounding radius
    D3DXVECTOR3 m_scale; // Scale
};
