Student Name:Louis Hong

Project Name:Don't F**cking Touch My Spaghet

What I implemented: Stop rats from touching your spaghet!

Directions (if needed): Click around and scare rats away from your spaghet.

What I liked about the project and framework: 
Behavior tree is semi-data driven.

What I disliked about the project and framework: 
There's mot enough comments. Figuring out how things are working together wasted a lot of my time. 
Forgetting to put nodes in procompiled header BehaviorTreesDef causes impossibly to decipher errors.
The X macros is very clever, the global singletons are nice.

Any difficulties I experienced while doing the project:
Forgetting to put nodes in procompiled header BehaviorTreesDef causes impossibly to decipher errors.

Hours spent: 21 hrs

New selector node (name):

REGISTER_LEAF(L_RunAwayFromCat, "Action: Run away from cat.")
REGISTER_LEAF(L_IsCloseToCat, "Condition: Is cat near.")
REGISTER_LEAF(L_RunAwayFromCheese, "Action: Run away from cheese.")
REGISTER_LEAF(L_EatCheese, "Action: Eat cheese.")
REGISTER_LEAF(L_GoHungry, "Action: Go hungry.")
REGISTER_LEAF(L_GoToCheese, "Action: Go to cheese.")
REGISTER_LEAF(L_MoveToClosestMouse, "Action: Go to closest mouse.")
REGISTER_LEAF(L_IsHungry, "Condition: Is hungry.")



New decorator nodes (names):
D_AlwaysTrue
D_RandomTrueFalse
D_RepeatTwice

10 total nodes (names):
D_AlwaysTrue
D_RandomTrueFalse
D_RepeatTwice

L_RunAwayFromCat)
L_IsCloseToCat
L_RunAwayFromCheese
L_EatCheese
L_GoHungry
L_GoToCheese
L_MoveToClosestMouse
L_IsHungry


4 Behavior trees (names):

Extra credit: